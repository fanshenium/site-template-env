# Frontend environment for projects

## Includes

* Webpack 3
* React-redux
* Bootstrap 3

Controlled by Gulp

## Setup

```
git clone git@gitlab.com:fanshenium/site-template-env.git <project_name>
npm install
```
**Take attention** on *output* parameter on the Webpack settings. It's configure for [django template](https://gitlab.com/fanshenium/site-template-django) by default.

## Run

Run for development build with Webpack-dev-server:

```
gulp
```

Run for production build:

```
gulp build
```

# Environment is combined with [django template](https://gitlab.com/fanshenium/site-template-django)

## Setup for [django template](https://gitlab.com/fanshenium/site-template-django)

```
git clone git@gitlab.com:fanshenium/site-template-env.git <project_name>
git clone git@gitlab.com:fanshenium/site-template-django.git <project_name>/project
pip install -r project/requirements.txt
npm install
```
