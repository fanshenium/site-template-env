import '../css/style.styl'

import './ajax'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader'

import store from './store'
import Router from './router'

ReactDOM.render(
  <AppContainer>
    <Provider store={ store }>
      <Router />
    </Provider>
  </AppContainer>,
  document.getElementById('root')
)
