const webpack           = require('webpack')
const path              = require('path')
const CompressionPlugin = require('compression-webpack-plugin')

const MODE = process.env.NODE_ENV

var publicPath = '/static/'
var entry = [
  'react-hot-loader/patch',
  'webpack-hot-middleware/client',
  'babel-polyfill',
  './assets/js/main.js'
]
var plugins = [
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(MODE)
  })
]
if (MODE == 'development') {
  publicPath = 'http://localhost:8001' + publicPath
  entry = [
    'webpack-dev-server/client?http://localhost:8001',
    'webpack/hot/only-dev-server'
  ].concat(entry)
  plugins = plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: true,
      minimize: false
    })
  ])
}

if (MODE == 'production') {
  plugins = plugins.concat([
    new webpack.LoaderOptionsPlugin({
      debug: false,
      minimize: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
        children: true,
        async: true,
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      parallel: true,
      sourceMap: false,
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false,
      },
    }),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|html)$/,
      threshold: 10240,
      minRatio: 0.8
    })
  ])
}

module.exports = {
  entry,
  output: {
    path: path.resolve(__dirname, 'project/project/static'),
    filename: 'js/script.js',
    publicPath,
    sourceMapFilename: '[file].map'
  },
  module: {
    rules: [
      {
        test: /\.styl$/,
        loaders: [
          'style-loader',
          {
            loader: 'postcss-loader',
            loader: 'css-loader',
            options: {
              sourceMap: true,
              plugins: () =>  [ require('autoprefixer')() ]
            }
          },
          {
            loader: 'stylus-loader',
            options: {
              paths: 'node_modules/bootstrap-styl'
            }
          }
        ]
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: [
          {
            loader: 'react-hot-loader/webpack'
          },
          {
            loader: 'babel-loader',
            options: {
              presets: [
                'react',
                ['es2015', {loose: true, modules: false}],
                'stage-0'
              ],
              cacheDirectory: ''
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: {
          loader: 'url-loader',
          options: {
            name: 'images/[name].[hash].[ext]',
            limit: 10000
          }
        }
      },
      {
        test: /\.(eot|ttg|ijmap|woff|woff2|svg|ttf)$/,
        use: {
          loader: 'url-loader',
          options: {
            name: 'fonts/[name].[hash].[ext]',
            limit: 100000
          }
        }
      }
    ]
  },
  resolve: {
    modules: [ 'node_modules' ],
    extensions: ['.js', '.jsx', '.css', '.scss']
  },
  devServer: {
    port: 8001,
    inline: true,
    compress: true,
    hot: true
  },
  devtool: '#inlune-source-map',
  plugins
}
